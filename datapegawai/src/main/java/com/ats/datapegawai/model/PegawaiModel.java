package com.ats.datapegawai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_PEGAWAI")
public class PegawaiModel {

	@Id
	@Column(name="NO")
	private int no;
	
	@Column(name="NM_PEGAWAI")
	private String namaPegawai;
	
	@Column(name="GJ_PEGAWAI")
	private int gajiPegawai;
	
	@Column(name="STATUS")
	private String status;
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getNamaPegawai() {
		return namaPegawai;
	}
	public void setNamaPegawai(String namaPegawai) {
		this.namaPegawai = namaPegawai;
	}
	public int getGajiPegawai() {
		return gajiPegawai;
	}
	public void setGajiPegawai(int gajiPegawai) {
		this.gajiPegawai = gajiPegawai;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
